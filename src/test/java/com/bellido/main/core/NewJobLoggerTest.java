package com.bellido.main.core;

import com.bellido.main.core.loggers.Logger;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Cesar on 8/26/2016.
 */
public class NewJobLoggerTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();

    }

    public void testLogMessage() throws Exception {
        String message = "new log message";
        NewJobLogger.logMessage(message);

        List<String> lines = null;
        try (BufferedReader br = Files.newBufferedReader(Paths.get("C://logs//myapp.log"))) {

            //br returns as stream and convert it into a List
            lines = br.lines().collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        String lastLine = lines.get(lines.size() - 1);

        assertTrue(lastLine.endsWith(message));
        assertTrue(lastLine.startsWith("MESSAGE"));
    }

    public void testLogWarning() throws Exception {
        String message = "new log message";
        NewJobLogger.logWarning(message);

        List<String> lines = null;
        try (BufferedReader br = Files.newBufferedReader(Paths.get("C://logs//myapp.log"))) {

            //br returns as stream and convert it into a List
            lines = br.lines().collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        String lastLine = lines.get(lines.size() - 1);

        assertTrue(lastLine.endsWith(message));
        assertTrue(lastLine.startsWith("WARNING"));
    }

    public void testLogError() throws Exception {
        String message = "new log message";
        NewJobLogger.logError(message);

        List<String> lines = null;
        try (BufferedReader br = Files.newBufferedReader(Paths.get("C://logs//myapp.log"))) {

            //br returns as stream and convert it into a List
            lines = br.lines().collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        String lastLine = lines.get(lines.size() - 1);

        assertTrue(lastLine.endsWith(message));
        assertTrue(lastLine.startsWith("ERROR"));
    }
}