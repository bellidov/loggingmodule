package com.bellido.main.util;

/**
 * Created by Cesar on 8/26/2016.
 */
public class Constants {
    private Constants(){}

    public static String BEANS_CONFIG = "beans.xml";
    public static String PROPERTIES_SOURCE = "properties";

    public static String MESSAGE = "MESSAGE";
    public static String WARNING = "WARNING";
    public static String ERROR = "ERROR";
}
