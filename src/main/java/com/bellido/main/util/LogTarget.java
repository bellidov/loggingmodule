package com.bellido.main.util;

/**
 * Created by Cesar on 8/25/2016.
 */
public enum LogTarget {
    CONSOLE,
    FILE,
    DATABASE;
}
