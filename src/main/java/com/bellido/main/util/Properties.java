package com.bellido.main.util;

import com.bellido.main.core.loggers.ConsoleLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

/**
 * Created by Cesar on 8/25/2016.
 */
@Component
public class Properties {

    @Value("${log.messages}")
    private boolean messagesLogged;

    @Value("${log.warnings}")
    private boolean warningsLogged;

    @Value("${log.errors}")
    private boolean errorsLogged;

    @Value("${target.console}")
    private boolean toConsole;

    @Value("${target.file}")
    private boolean toFile;

    @Value("${target.db}")
    private boolean toDatabase;

    @Value("${file.path}")
    private String filePath;

    public boolean isMessagesLogged() {
        return messagesLogged;
    }

    public boolean isWarningsLogged() {
        return warningsLogged;
    }

    public boolean isErrorsLogged() {
        return errorsLogged;
    }

    public boolean isToConsole() {
        return toConsole;
    }

    public boolean isToFile() {
        return toFile;
    }

    public boolean isToDatabase() {
        return toDatabase;
    }

    public String getFilePath() {
        return filePath;
    }
}
