package com.bellido.main.util;

import com.bellido.main.core.loggers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Cesar on 8/26/2016.
 */
@Component
public class LoggerFabric {

    @Autowired
    private static ConsoleLogger consoleLogger;

    private LoggerFabric(){}

    //small fabric
    public static Logger get(LogTarget target){
        Logger logger = new DummyLogger();
        switch(target){
            case CONSOLE:
                logger = new ConsoleLogger();
                break;
            case FILE:
                logger = new FileLogger();
                break;
            case DATABASE:
                logger = new DBLogger();
                break;
        }
        return logger;
    }
}
