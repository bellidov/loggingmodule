package com.bellido.main.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.SessionFactoryBuilder;

/**
 * Created by Cesar on 8/26/2016.
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getInitSessionFactory(){
        try{
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().
                    configure("hibernate.cfg.xml").build();

            Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().
                    build();

            SessionFactoryBuilder sessionFactoryBuilder = metadata.getSessionFactoryBuilder();

            sessionFactory = sessionFactoryBuilder.build();
        }
        catch(Exception e){
        }

        return sessionFactory;
    }

    public static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            sessionFactory = getInitSessionFactory();
        }
        return sessionFactory;
    }

    public static void closeSession(){
        getSessionFactory().close();
    }
}
