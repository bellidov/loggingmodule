package com.bellido.main.core;

import com.bellido.main.util.Constants;
import com.bellido.main.util.LogTarget;
import com.bellido.main.util.LoggerFabric;
import com.bellido.main.util.Properties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Cesar on 8/24/2016.
 */
public class NewJobLogger {
    private static final String BEANS_CONFIG_FILE = Constants.BEANS_CONFIG;
    private static final String PROPERTIES_FILE = Constants.PROPERTIES_SOURCE;
    private static ApplicationContext context = new ClassPathXmlApplicationContext(BEANS_CONFIG_FILE);
    public static final Properties props = (Properties)context.getBean(PROPERTIES_FILE);
    private static LogTarget[] targets = null;

    private NewJobLogger(){}

    public static void logMessage(String message, LogTarget ... targets){
        if(!props.isMessagesLogged()){return;}
        saveLog(message, Constants.MESSAGE, targets);
    }

    public static void logWarning(String message, LogTarget ... targets){
        if(!props.isWarningsLogged()){return;}
        saveLog(message, Constants.WARNING, targets);
    }

    public static void logError(String message, LogTarget ... targets){
        if(!props.isErrorsLogged()){return;}
        saveLog(message, Constants.ERROR, targets);
    }

    private static void saveLog(String message, String level, LogTarget ... targets){
        message = level + " " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + ": " + message;
        if(targets.length != 0){
            for(LogTarget target : targets){
                LoggerFabric.get(target).log(message);
            }
        }
        else{   // temporal
            if(props.isToConsole()){
                LoggerFabric.get(LogTarget.CONSOLE).log(message);
            }
            if(props.isToFile()){
                LoggerFabric.get(LogTarget.FILE).log(message);
            }
            if(props.isToDatabase()){
                LoggerFabric.get(LogTarget.DATABASE).log(message);
            }
        }
    }
}