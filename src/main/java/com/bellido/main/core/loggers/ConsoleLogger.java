package com.bellido.main.core.loggers;

import org.springframework.stereotype.Service;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

/**
 * Created by Cesar on 8/26/2016.
 */
public class ConsoleLogger implements Logger {

    private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("MyLog");

    public ConsoleLogger(){
      //  ConsoleHandler ch = new ConsoleHandler();
       // logger.addHandler(ch);
    }

    public void log(String message) {
        logger.log(Level.INFO, message);
    }
}
