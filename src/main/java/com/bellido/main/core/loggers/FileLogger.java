package com.bellido.main.core.loggers;

import com.bellido.main.core.NewJobLogger;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Level;

/**
 * Created by Cesar on 8/26/2016.
 */
public class FileLogger implements Logger {
    private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger("MyLog");

    public FileLogger(){
   /*     try {
            FileHandler fh = new FileHandler(NewJobLogger.props.getFilePath());
            logger.addHandler(fh);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void log(String message) {
       // logger.log(Level.INFO, message);

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NewJobLogger.props.getFilePath(), true)))) {
            out.println(message);
        }catch (IOException e) {
            System.err.println(e);
        }
    }
}
