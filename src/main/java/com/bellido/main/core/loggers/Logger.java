package com.bellido.main.core.loggers;

/**
 * Created by Cesar on 8/26/2016.
 */
public interface Logger {
    void log(String message);
}
